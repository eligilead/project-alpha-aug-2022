This is a project and task manager application. It is composed of projects, tasks, and the ability to login, logout, and sign up for the application.
 
Authenticated users have the ability to create projects and can share access to their projects with whoever in their network they deem fit.
