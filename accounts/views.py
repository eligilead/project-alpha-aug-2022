from django.shortcuts import render, redirect
from django.contrib.auth.views import LoginView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login


# Create your views here.


class LoginView(LoginView):
    template_name = "registration/login.html"


def signup(request):
    if request.method == "POST":
        signup_request = UserCreationForm(request.POST)
        if signup_request.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()
            login(request, user)
            return redirect("home")
    else:
        signup_request = UserCreationForm()
    context = {"form": signup_request}
    return render(request, "registration/signup.html", context)
