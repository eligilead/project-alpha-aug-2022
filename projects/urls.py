from django.urls import path

from projects.views import (
    ProjectListView,
    # ProjectDetailView,
    ProjectCreateView,
    projectdetail,
)

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:pk>/", projectdetail, name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
]
