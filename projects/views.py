from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from projects.models import Project

# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"
    context_object_name = "project_list"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)

    def __str__(self):
        return str(self.name)


# class ProjectDetailView(LoginRequiredMixin, DetailView):
#     model = Project
#     template_name = "projects/detail.html"
#     context_object_name = "project_detail"


def projectdetail(request, pk):
    project = Project.objects.get(pk=pk)
    context = {
        "project": project  # holds a instance of the class Project model
    }
    return render(request, "projects/detail.html", context)


#                 object,   string,               dict.

"""

get somehing from db, and create a context dict. render template
every key in the context dict. is a var that can be used in the html temp.
"""


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]
    context_object_name = "project_create"

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


# def projectcreate(request):
#     if request.method == "POST":

#     else:


"""
if its a post
    we need to check to see if the form is valid
    if the form is valid
        we want to save the project to the database
        then redirect user to project detail page, that they created
    if the form is not valid render the form again
if its a get request 
    just render the form
"""
